package com.animushome.heart.packages.examplepackage;


import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;
import org.osgi.service.event.EventAdmin;

import com.animushome.heart.service.Package;
import com.animushome.heart.service.dal.impl.PackageImpl;

@Component(immediate=true, service=Package.class)
public class ExamplePackage extends PackageImpl {
	
	//The device registration
	private ServiceRegistration<?> deviceReg;
	
	//The function registration
	private ServiceRegistration<?> funcReg;
	
	/**
	 * Activate is called when the package has all the requirements fulfilled
	 * @param context
	 */
	@Activate
    protected void activate(ComponentContext context) {
      construct("ExamplePackage", context);
    }
	
	/**
	 * Deactivate is called when the package gets uninstalled. We should 
	 * unregister all the services that we have registered.
	 * @param context
	 */
	@Deactivate
    protected void deactivate(ComponentContext context) {
      logger.info("Example package is being deactivated");
      if (deviceReg != null) {
    	  deviceReg.unregister();
      }
      if (funcReg != null) {
    	  funcReg.unregister();
      }
    }
	
	/**
	 * If the device is auto detected it should be registered when this method is called.
	 */
	@Override
	public void scan() {
		logger.info("Found MyDevice");
		MyDevice d = new MyDevice(this);
		deviceReg = registerDevice(d);
		
		funcReg = registerFunction(new MyFunction(d.DEVICE_UID, this));
		
	}
	
	/**
	 * We use Declarative Services to get the EventAdmin so that our 
	 * functions can post events on the event bus when their states changes. 
	 * @param ea
	 */
	@Reference
    void setEventAdmin(EventAdmin ea) {
      this.eventAdmin = ea;
    }

}
